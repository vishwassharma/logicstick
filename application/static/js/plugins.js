(function() {
  var Obj, main_map, otherui;

  Obj = (function() {

    function Obj(marker_index, shadow_index) {
      this.marker_index = marker_index;
      this.shadow_index = shadow_index;
      this.type = 'vector';
      this.format = 'kml';
      this.selectable = true;
      this.kml = {
        protocol: 'http',
        url: '/maps/sources',
        strategies: ['fixed']
      };
      this.style = {
        "default": {
          externalGraphic: '',
          backgroundGraphic: '',
          pointRadius: 11,
          graphicZIndex: this.marker_index,
          backgroundXOffset: 0,
          backgroundYOffset: -7,
          backgroundGraphicZIndex: this.shadow_index,
          cursor: 'pointer'
        },
        select: {
          externalGraphic: '',
          backgroundGraphic: '',
          pointRadius: 11,
          graphicZIndex: this.marker_index,
          backgroundXOffset: 0,
          backgroundYOffset: -7,
          backgroundGraphicZIndex: this.shadow_index,
          cursor: 'pointer'
        },
        temporary: {
          externalGraphic: '',
          backgroundGraphic: '',
          pointRadius: 13,
          graphicZIndex: this.marker_index,
          backgroundXOffset: 0,
          backgroundYOffset: -7,
          backgroundGraphicZIndex: this.shadow_index,
          cursor: 'pointer'
        }
      };
    }

    return Obj;

  })();

  main_map = function() {
    var $map, $plot, $slider, a, destination, marker_index, opt, path, shadow_index, source;
    shadow_index = 10;
    marker_index = 11;
    source = new Obj(marker_index, shadow_index);
    destination = new Obj(marker_index + 1, shadow_index + 1);
    path = new Obj(marker_index + 2, shadow_index + 2);
    path.kml.url = '/maps/path/';
    path.hasSlider = true;
    delete path.style;
    path.style = {
      "default": {
        backgroundGraphicZIndex: 4,
        graphicZIndex: 5,
        cursor: 'pointer',
        strokeColor: '#00A2E8',
        strokeWidth: 6,
        strokeOpacity: 0.75
      }
    };
    opt = {
      map: {
        type: 'google'
      },
      source: source,
      destination: destination,
      path: path
    };
    $map = $('#map');
    $map.logicstick({});
    a = $map.data('logicstick');
    a.addLayer(options['map']);
    a.setCenter({
      zoom: 6
    });
    $slider = $('#h-slider');
    $slider.Slider({
      map: '#map'
    });
    $plot = $('#plot');
    $plot.Chart({
      map: '#map'
    });
    a.addLayer(options['path']);
    return a.addLayer(options['source']);
  };

  otherui = function() {
    var $accordian;
    $accordian = $('#accordian');
    return $accordian.dashboard({
      accordian: 'slideup',
      other: false
    });
  };

  jQuery(function($) {
    main_map();
    return otherui();
  });

}).call(this);
