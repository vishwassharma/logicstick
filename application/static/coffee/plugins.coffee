class Obj
  constructor : (@marker_index, @shadow_index) ->
    @type = 'vector'
    @format = 'kml'
    @selectable = true
    @kml =
      protocol : 'http'
      url : '/maps/sources'
      strategies : ['fixed',]
      # extractStyles : true
    @style =
      default :
        externalGraphic : ''
        backgroundGraphic : ''
        pointRadius : 11
        # strokeWidth : 11
        # strokeColor : '#00A2E8'
        graphicZIndex : @marker_index
        backgroundXOffset : 0
        backgroundYOffset : -7
        backgroundGraphicZIndex : @shadow_index
        cursor : 'pointer'
      select :
        externalGraphic : ''
        backgroundGraphic : ''
        pointRadius : 11
        # strokeWidth : 11
        # strokeColor : '#00A2E8'
        graphicZIndex : @marker_index
        backgroundXOffset : 0
        backgroundYOffset : -7
        backgroundGraphicZIndex : @shadow_index
        cursor : 'pointer'
      temporary :
        externalGraphic : ''
        backgroundGraphic : ''
        pointRadius : 13
        graphicZIndex : @marker_index
        backgroundXOffset: 0
        backgroundYOffset : -7
        backgroundGraphicZIndex : @shadow_index
        cursor : 'pointer'

main_map = ->
  shadow_index = 10
  marker_index = 11
 
  source = new Obj marker_index, shadow_index
  destination = new Obj marker_index+1, shadow_index+1
  path = new Obj marker_index+2, shadow_index+2

  path.kml.url = '/maps/path/'
  path.hasSlider = true
  delete path.style
  path.style = 
    default :
      backgroundGraphicZIndex : 4
      graphicZIndex : 5
      cursor : 'pointer'
      strokeColor : '#00A2E8'
      strokeWidth : 6
      strokeOpacity : 0.75
  opt =
    map :
      type : 'google'
    source : source
    destination : destination
    path : path
  
  # logicstick plugin
  $map = $('#map')
  $map.logicstick {}
  a = $map.data 'logicstick'
  a.addLayer options['map']
  a.setCenter {zoom : 6}
  
  # Slider addon
  $slider = $('#h-slider')
  $slider.Slider { map : '#map'}

  # Plot addon
  $plot = $('#plot')
  $plot.Chart { map : '#map'}

  a.addLayer options['path']
  a.addLayer options['source']

otherui = ->
  $accordian = $('#accordian')
  $accordian.dashboard {
    accordian : 'slideup'
    other : false
  }

jQuery ($) ->
  do main_map
  do otherui
