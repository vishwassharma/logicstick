'''
File: views.py
Author: Vishwas Sharma
Description: Views
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect

@login_required
def index(request):
    ''' 
    Index page
    '''    
    content = {}
    return HttpResponseRedirect('%s/' % request.user.username)

@login_required
def landing_page(request, username=None):
    '''
    LANDING PAGE FOR THE DASHBOARD
    '''
    content = {}
    return render_to_response('fleet/track/index.html', content, RequestContext(request))
