'''
File: urls.py
Author: Vishwas Sharma
Description: Urls for / page
'''
from django.conf.urls.defaults import *

urlpatterns = patterns('application.apps.track.views',
    # Main
    url(r'^$', 'index', name='track_index'),
    url(r'^(\w+)/$', 'landing_page', name='track_page'),
)
