from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class UserProfile(models.Model):
  user = models.OneToOneField(User)
  company = models.CharField(max_length=200, blank=True, null=True)
  address = models.TextField(blank=True, null=True)
  
  def __unicode__(self):
      return self.user.username
    
