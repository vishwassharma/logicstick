'''
Created on 23-Jan-2012

@author: Vishwas
'''
from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('application.apps.profile.views',
  # url(r'^myapp/', include('myapp.urls')),
  
  # Home Page -- Replace if you like
  # url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
  #url(r'^$', 'accounts_index', name='act_index'),
)

urlpatterns += patterns('',
                        url(r'^login/$', 'django.contrib.auth.views.login', name='act_login'),
                        url(r'^logout/$', 'django.contrib.auth.views.logout',{'next_page' : '/'} ,name='act_logout'),
                        )
