'''
File: urls.py
Author: Vishwas Sharma
Description: Urls for / page
'''


def about(request):
  """docstring for about"""
  return render_to_response('generic/about.html', {
      
    }, context_instance=RequestContext(request)
  )

def contact(request):
  """docstring for contact"""
  return render_to_response('generic/contact.html', {
      
    }, context_instance=RequestContext(request)
  )


def test(self):
  """docstring for test"""
  return render_to_response('generic/text.html', {
      
    }, context_instance=RequestContext(request)
  )
