import os
from django.contrib.auth import logout
from django.http import Http404
from django.shortcuts import redirect, get_object_or_404, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.template.context import RequestContext

#from annoying.decorators import render_to, ajax_request
#from annoying.functions import get_object_or_None, get_config

#from main.forms import *
#from main.models import *

#@render_to('main/index.html')

def about(request):
    ''' 
    Index page
    '''    
    content = {}
    return render_to_response('generic/about.html', content, RequestContext(request))


def contact(request):
    ''' 
    Index page
    '''    
    content = {}
    return render_to_response('generic/contact.html', content, RequestContext(request))

@login_required
def test(request):
    ''' 
    Index page
    '''    
    content = {}
    return render_to_response('generic/test.html', content, RequestContext(request))