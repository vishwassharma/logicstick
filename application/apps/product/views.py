'''
File: views.py
Author: Vishwas Sharma
Description: Views
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext

def product_index(request):
  """docstring for product_index"""
  return render_to_response('product/index.html', {
      
    }, context_instance=RequestContext(request)
  ) 
