'''
File: urls.py
Author: Vishwas Sharma
Description: URLS
'''

from django.conf.urls.defaults import *
urlpatterns = patterns('application.apps.product.views',
    url(r'^$', 'product_index', name='product_index'),
)
