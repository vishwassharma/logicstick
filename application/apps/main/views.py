'''
File: views.py
Author: Vishwas Sharma
Description: Views
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext

def index(request):
  """Index Page"""
  content = {}
  return render_to_response('base.html', content, RequestContext(request))

def error404(request):
  """handle 404 error"""
  context = {}
  return render_to_response('404.html', content, context_instance=RequestContext(request))
