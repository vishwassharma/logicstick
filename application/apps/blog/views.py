'''
File: views.py
Author: Vishwas Sharma
Description: Views
'''
from django.shortcuts import render_to_response
from django.template.context import RequestContext

def blog_index(request):
  """docstring for blog_index"""
  return render_to_response('blog/index.html', {
      
    }, context_instance=RequestContext(request)
  )
  
