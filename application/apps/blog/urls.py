'''
File: urls.py
Author: Vishwas Sharma
Description: Urls for / page
'''
from django.conf.urls.defaults import *
urlpatterns = patterns('application.apps.blog.views',
    url(r'^$', 'blog_index', name='blog_index'),
)
