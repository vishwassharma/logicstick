from django.conf.urls.defaults import patterns, url, include, handler404

urlpatterns = patterns('',
    # project urls here
    url(r'^$', 'application.apps.main.views.index'),
    url(r'^product/', include('application.apps.product.urls')),
    url(r'^blog/', include('application.apps.blog.urls')),
    
    url(r'^contact/', 'application.apps.generic.views.contact'),
    url(r'^about/', 'application.apps.generic.views.about'),
    
    url(r'^profile/', include('application.apps.profile.urls')),
    url(r'^track/', include('application.apps.track.urls')),

    url(r'^test/', 'application.apps.generic.views.test'),
)


handler404 = 'application.apps.main.views.error404'
