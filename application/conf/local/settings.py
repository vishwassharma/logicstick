from application.conf.settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('You', 'your@email'),
)
MANAGERS = ADMINS

DATABASES = {
  'default': {
      'ENGINE': 'django.db.backends.sqlite3',
      'NAME': os.path.join(VAR_ROOT, 'dev.db'),
  }
}

ROOT_URLCONF = '%s.conf.local.urls' % PROJECT_MODULE_NAME

# Debug Toolbar
INTERNAL_IPS = ('127.0.0.1',)

def custom_show_toolbar(request):
  return True # Always show toolbar, for example purposes only.

DEBUG_TOOLBAR_CONFIG = {
  'INTERCEPT_REDIRECTS': False,
  'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
  'HIDE_DJANGO_SQL': False,
  'TAG': 'div',
  'ENABLE_STACKTRACES' : True,
}

MIDDLEWARE_CLASSES += (
  'debug_toolbar.middleware.DebugToolbarMiddleware',
)
# ---------------

INSTALLED_APPS += (
  'django.contrib.admin',
  'django.contrib.admindocs',
  'django.contrib.webdesign',
  'debug_toolbar',
)

DEBUG_TOOLBAR_PANELS = (
  'debug_toolbar.panels.version.VersionDebugPanel',
  'debug_toolbar.panels.timer.TimerDebugPanel',
  'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
  'debug_toolbar.panels.headers.HeaderDebugPanel',
  'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
  'debug_toolbar.panels.template.TemplateDebugPanel',
  'debug_toolbar.panels.sql.SQLDebugPanel',
  'debug_toolbar.panels.signals.SignalDebugPanel',
  'debug_toolbar.panels.logger.LoggingPanel',
)
